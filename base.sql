CREATE SEQUENCE sq_customer
START WITH 1
INCREMENT BY 1;

CREATE TABLE customer(
id int UNIQUE NOT NULL,
  name varchar2(50) NOT NULL,
  email varchar2(50) UNIQUE NOT NULL,
  password varchar2(50) NOT NULL,
  money int NULL
);

CREATE OR REPLACE TRIGGER trigger_customer
BEFORE INSERT ON customer  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_customer.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_customer(name1 varchar2, email1 varchar2, password1 varchar2)
AS
BEGIN
INSERT INTO customer(name, email, password) VALUES(name1, email1, password1);
END;
/





CREATE SEQUENCE sq_ordering_status
START WITH 1
INCREMENT BY 1;

CREATE TABLE ordering_status(
ID int UNIQUE,
name varchar2(30) UNIQUE
);

CREATE OR REPLACE TRIGGER trigger_ordering_status
BEFORE INSERT ON ordering_status  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_ordering_status.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_ordering_status(name1 varchar2)
AS
BEGIN
INSERT INTO ordering_status(name) VALUES(name1);
END;
/






CREATE SEQUENCE sq_supplier
START WITH 1
INCREMENT BY 1;

CREATE TABLE supplier(
ID int UNIQUE,
name varchar2(50)
);

CREATE OR REPLACE TRIGGER trigger_supplier
BEFORE INSERT ON supplier  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_supplier.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_supplier(name1 varchar2)
AS
BEGIN
INSERT INTO supplier(name) VALUES(name1);
END;
/






CREATE SEQUENCE sq_product_status
START WITH 1
INCREMENT BY 1;

CREATE TABLE product_status(
ID int UNIQUE,
name varchar2(50)
);

CREATE OR REPLACE TRIGGER trigger_product_status
BEFORE INSERT ON product_status  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_product_status.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_product_status(name1 varchar2)
AS
BEGIN
INSERT INTO product_status(name) VALUES(name1);
END;
/






CREATE SEQUENCE sq_category
START WITH 1
INCREMENT BY 1;

CREATE TABLE category(
ID int UNIQUE,
name varchar2(50)
);

CREATE OR REPLACE TRIGGER trigger_category
BEFORE INSERT ON category  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_category.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_category(name1 varchar2)
AS
BEGIN
INSERT INTO category(name) VALUES(name1);
END;
/








CREATE SEQUENCE sq_ordering
START WITH 1
INCREMENT BY 1;

CREATE TABLE ordering(
ID int UNIQUE NOT NULL,
total_price int NOT NULL,
customer_id int,
status_id int,
amount int,
CONSTRAINT fk_ordering_customer FOREIGN KEY (customer_id )
REFERENCES customer(ID),
CONSTRAINT fk_ordering_status FOREIGN KEY (status_id )
REFERENCES ordering_status(ID)
);

CREATE OR REPLACE TRIGGER trigger_ordering
BEFORE INSERT ON ordering  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_ordering.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_ordering(price int, customer int, status int, sum int)
AS
BEGIN
INSERT INTO ordering(total_price,customer_id, status_id, amount) VALUES(price, customer, status, sum);
END;
/









CREATE SEQUENCE sq_transaction
START WITH 1
INCREMENT BY 1;

CREATE TABLE transaction(
ID int UNIQUE,
money int,
customer_id int,
ordering_id int,
CONSTRAINT fk_transaction_customer FOREIGN KEY (customer_id)
REFERENCES customer(ID),
CONSTRAINT fk_transaction_ordering FOREIGN KEY (ordering_id)
REFERENCES ordering(ID)
);

CREATE OR REPLACE TRIGGER trigger_transaction
BEFORE INSERT ON transaction  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_transaction.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_transaction(price int, customer int, ordering int)
AS
BEGIN
INSERT INTO transaction(money,customer_id, ordering_id) VALUES(price, customer, ordering);
END;
/







CREATE SEQUENCE sq_supply
START WITH 1
INCREMENT BY 1;

CREATE TABLE supply(
ID int UNIQUE,
amount int,
price int,
supplier_id int,
CONSTRAINT fk_supply_supplier FOREIGN KEY (supplier_id)
REFERENCES supplier(ID)
);

CREATE OR REPLACE TRIGGER trigger_supply
BEFORE INSERT ON supply  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_supply.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_supply(amount_of_supply int, money int, supplier int)
AS
BEGIN
INSERT INTO supply(amount,price, supplier_id) VALUES(amount_of_supply, money, supplier);
END;
/








CREATE SEQUENCE sq_product
START WITH 1
INCREMENT BY 1;

CREATE TABLE product(

ID int UNIQUE,
name varchar2(30),
price int,
amount int,

supply_id int,
status_id int,
category_id int,

CONSTRAINT fk_product_supply FOREIGN KEY (supply_id)
REFERENCES supply(ID),

CONSTRAINT fk_product_status FOREIGN KEY (status_id)
REFERENCES product_status(ID),

CONSTRAINT fk_product_category FOREIGN KEY (category_id)
REFERENCES category(ID)
);

CREATE OR REPLACE TRIGGER trigger_product
BEFORE INSERT ON product  
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_product.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_product(appellation varchar2, money int, sum int, supply int, status int, category int)
AS
BEGIN
INSERT INTO product(
name ,
price ,
amount ,
supply_id ,
status_id ,
category_id
) VALUES(
    appellation,
    money,
    sum,
    supply,
    status,
    category
);
END;
/











CREATE SEQUENCE sq_ordering_item
START WITH 1
INCREMENT BY 1;

CREATE TABLE ordering_item(
ID int UNIQUE,
product_id int,
ordering_id int,
CONSTRAINT fk_ordering_item_product FOREIGN KEY (product_id)
REFERENCES product(ID),
CONSTRAINT fk_ordering_item_ordering FOREIGN KEY (ordering_id)
REFERENCES ordering(ID)
);

CREATE OR REPLACE TRIGGER trigger_ordering_item
BEFORE INSERT ON ordering_item
FOR EACH ROW
BEGIN  
IF :new.ID IS NULL THEN  
:new.ID := sq_ordering_item.NEXTVAL;
END IF;
END;
/

CREATE OR REPLACE PROCEDURE add_ordering_item(ordering int, product int)
AS
BEGIN
INSERT INTO ordering_item(
product_id ,
ordering_id
) VALUES(
    product,
    ordering
    );
END;
/
