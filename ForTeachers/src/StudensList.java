import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;

public class StudensList {
    private String group ="Нет группы";
    private ArrayList<Student> students;
    private int idCounter = 1;


    public String getName(int id){
        for(Student s: students){
            if(s.getId()==id){
                return s.getName();
            }
        }
        return "Неверный id";
    }

    public void StudentsList() {
        this.students = new ArrayList<>();
    }

    public void add(Student s) {
        s.setId(this.idCounter++);
        students.add(s);
    }


    public void update(int id, String newname ) { //id=2, name="qwe"
        Student s;
        for(int i=0;i<students.size();i++){
            if(students.get(i).getId()==id){
                s=students.get(i);
                s.setName(newname);
                students.set(i,s);
            }
        }
    }

   /*public void remove(int id) {
       Iterator<Student> iter = this.students.iterator();
       while (iter.hasNext()) {
           Student s = iter.next();
           if (s.getId()==id) {
               iter.remove();
           }
       }
   }*/

    public void remove(int id) {
        int ind=-1;
        for(int i=0;i<students.size();i++){
            if(students.get(i).getId()==id){
                ind =i;
            }
        }
        if(ind>=0 && ind<students.size()){
            students.remove(ind);
        }
    }

    public Student getStudent(int id) {
        for(int i=0;i<students.size(); i++){
            if(students.get(i).getId()==id){
                return students.get(i);
            }
        }
        return null;
    }

    public ArrayList<Student> getStudents() {
        return this.students;
    }


    public void getNewInformation(BufferedWriter bw){
        if(students.size()==0){
            return;
        }else if(students.size()==1){
            try {
                bw.write(students.get(0).toString()+"\n");
            }catch (Exception ex){
                //System.out.println("Что-то случилось: "+ex.getMessage());
            }
            return;
        }
        try {
            for (int i = 0; i < students.size(); i++) {
                bw.write(students.get(i).toString()+"\n");
            }
        }catch(Exception ex){
            //System.out.println("Что-то случилось: "+ex.getMessage());
        }
    }


    public static StudensList UpdateInformation(BufferedReader br) {
        StudensList list = new StudensList();
        list.StudentsList();
        String str = null;
        while (true) {
            try {
                str = br.readLine();
            } catch (Exception ex) {
                System.out.println("Что-то случилось не так: " + ex.getMessage());
            }
            if(str==null || str == ""){
                break;
            }
            list.add(Student.fromString(str));

        }
        return list;
    }


}




