import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static Scanner scan = new Scanner(System.in);
    public static Random rand = new Random();

    public static void main(String[] args) throws Exception {
        StudensList list = new StudensList();
        list.StudentsList();
        FileReader fr;
        while (true){
            System.out.print("С каким файлом будем работать: ");
            String filenameInp = scan.nextLine();
            String filename = "";
            for(int i=0;i<(filenameInp.length()-1);i++){
                filename+=filenameInp.charAt(i);
                if(filenameInp.charAt(i+1)=='\\'){
                    filename+="\\";
                }
            }
            filename+=filenameInp.charAt(filenameInp.length()-1);
            BufferedReader br = null;
            fr=new FileReader(filename);
            try{
                br= new BufferedReader(new FileReader(filename));
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
            list = list.UpdateInformation(br);
            boolean UpdateInp = false;
            BufferedWriter bw = new BufferedWriter(Writer.nullWriter());
            try {
                bw = new BufferedWriter(new FileWriter(filename));
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }

            boolean closeInd = true;
            while (true) {
                if(UpdateInp) {
                    br = new BufferedReader(new FileReader(filename));
                    list = list.UpdateInformation(br);
                }
                UpdateInp=true;
                System.out.println("1 - Записать студента\n" +
                        "2 - Изменить студента\n" +
                        "3 - Вывести студентов\n" +
                        "4 - Вывести студента по id\n" +
                        "5 - Удалить студента по id\n" +
                        "6 - Записать оценки студента\n" +
                        "7 - Вывести среднюю оценку студента по id\n" +
                        "8 - Закрыть документ");
                System.out.print("Ваше действие:");
                String inp = scan.nextLine();

                if (inp.equals("")) {
                    inp = scan.nextLine();
                } else if (inp.equals("8")) {
                    bw.flush();
                    bw.close();
                    br.close();
                    closeInd = false;
                    break;
                }
                menu(inp, list);
                try {
                    bw = new BufferedWriter(new FileWriter(filename));
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                list.getNewInformation(bw);
                if (closeInd) {
                    bw.flush();
                }

            }
        }
    }


    public static void menu(String inp, StudensList list) {
        switch (inp){
            case "1":
                Student s= new Student();
                System.out.println("Введите его(её) имя и фамилию: ");
                String name = scan.nextLine();
                s.setName(name);
                System.out.println("Введите её группу: ");
                String group = scan.nextLine();
                s.setGroup(group);
                list.add(s);
                break;
            case "2":
                Student s2= new Student();
                System.out.print("Введите id студента, которого хотите изменить:");
                int id= scan.nextInt();
                if(list.getStudent(id)!=null){
                    s2= list.getStudent(id);
                }else{
                    System.out.println("Нет такого студента");
                    break;
                }
                while(true) {
                    System.out.println("Что вы хотите изменить?\n" +
                            "1 - Имя и Фамилию\n" +
                            "2 - Группу\n" +
                            "3 - Оценки\n" +
                            "4 - Выйти");
                    System.out.print("Ваше действие:");
                    String deis = scan.nextLine();
                    if(deis.equals("")){
                        deis = scan.nextLine();
                    }
                    if (deis.equals("4")){
                        break;
                    }
                    switch (deis) {
                        case "1":
                            System.out.print("Ваше текущее имя:");
                            System.out.println(s2.getName());
                            System.out.println("Изменить его на(если вы не хотите его поменять надо ввести пустую строку): ");
                            String newname = scan.nextLine();
                            if(newname.equals("")){
                                continue;
                            }else{
                                list.getStudent(id).setName(newname);
                            }
                            break;
                        case "2":
                            System.out.print("Ваша текущая группа: ");
                            System.out.println(s2.getGroup());
                            System.out.println("Изменить его на(если вы не хотите его поменять надо ввести пустую строку): ");
                            String newgroup = scan.nextLine();
                            if(newgroup.equals("")){
                                continue;
                            }else{
                                list.getStudent(id).setGroup(newgroup);
                            }
                            break;
                        case "3":
                            System.out.print("Ваши текующие оценки:");
                            s2.soutOzeni();
                            System.out.println();
                            System.out.print("Если вы не хотите его поменять надо ввести пустую строку\n" +
                                    "Какую оценку поменять(чтобы их перезаписать полностью введите all): ");
                            String nomer = scan.nextLine();
                            if(correct(nomer).equals("all")){
                                list.getStudent(id).removeOzenki();
                                System.out.print("Сколько оценок будете вводить: ");
                                int colvo = scan.nextInt();
                                System.out.print("Вводите новые оценки: ");
                                list.getStudent(id).removeOzenki();
                                for(int i=0;i<colvo;i++){
                                    list.getStudent(id).ozenki.add(scan.nextInt());
                                }
                            }else{
                                try{
                                    Integer.parseInt(nomer);
                                }catch (Exception ex){
                                    System.out.println("Неправильный ввод");
                                    break;
                                }
                                System.out.print("Новая оценка: ");
                                list.getStudent(id).ozenki.set(Integer.parseInt(nomer)-1, scan.nextInt());
                            }
                            break;
                        default:
                            System.out.println("Неправильный ввод");
                            break;
                    }
                }
                break;
            case "3":
                for(Student x : list.getStudents()){
                    System.out.println("Имя:"+x.getName());
                    System.out.println("Группа:"+x.getGroup());
                    System.out.println("id:"+x.getId());
                }
                break;
            case "4":
                System.out.print("Введите id:");
                int id4= scan.nextInt();
                if(list.getStudent(id4)==null){
                    System.out.println("Нет такого студента");
                    break;
                }
                System.out.println("Имя:"+list.getStudent(id4).getName());
                System.out.println("Группа:"+list.getStudent(id4).getGroup());
                System.out.println("id:"+list.getStudent(id4).getId());
                break;
            case "5":
                System.out.print("Введите id:");
                int id5= scan.nextInt();
                if(list.getStudent(id5)==null){
                    System.out.println("Нет такого студента");
                    break;
                }
                list.remove(id5);
                break;
            case "6":
                System.out.print("Введите id ученика:");
                int id6= scan.nextInt();
                if(list.getStudent(id6)==null){
                    System.out.println("Такого студента не существует");
                    break;
                }
                list.getStudent(id6).spawnOzenki();
                System.out.print("Сколько оценок будете вводить, если не хотите - введите -1:");
                int colvo= scan.nextInt();
                if(colvo<=0 && colvo!=-1){
                    System.out.println("Неправильный ввод");
                    break;
                }
                if(colvo==-1){
                    break;
                }else{
                    System.out.print("Вводите оценки(от 2 до 5)");
                    for(int i=0;i<colvo;i++){
                        list.getStudent(id6).passExam(scan.nextInt());
                    }
                }
                break;
            case "7":
                System.out.print("Введите id ученика:");
                int id7= scan.nextInt();
                if(list.getStudent(id7)==null){
                    System.out.println("Такого студента не существует");
                    break;
                }
                int sum=0;
                for(int x:list.getStudent(id7).ozenki){
                    sum+=x;
                }
                System.out.println("Средняя оценка студента "+list.getStudent(id7).getName()+" равна "+ (1.0*sum/(list.getStudent(id7).ozenki.size())));
                break;
            default:
                System.out.println("Неправильный ввод");
        }
    }

    public static String correct(String str) {
        String itog="";
        if(str.equals("all")||str.equals("All")){
            return "all";
        }
        int x;
        for(int i=0;i<str.length();i++){
            char symb = str.charAt(i);
            if((symb>='0' && symb<='9') || symb==' '){
                itog+=symb;
            }else{
                return "";
            }
        }
        if(itog.length()>0){
            x=Integer.parseInt(itog);
        }else{
            return "";
        }
        return itog;
    }
}

