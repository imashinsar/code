import java.util.ArrayList;

public class Student {
    private int id;
    private int i=0;
    public int sum=0;
    public ArrayList<Integer> ozenki;
    public static int GET_STUDENT_COUNT = 0;
    public static int GET_STUDENT_SUM = 0;
    private String group ="Нет группы";
    private String name;

    public Student(){
        name ="noname";
    }

    public int getId() {
        return id;
    }

    public String getGroup(){
        return group;
    }

    public final void  setId(int id) {
        this.id = id;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setGroup(String group){
        this.group=group;
    }

    public void spawnOzenki(){
        this.ozenki= new ArrayList<>();
    }

    public void passExam(int ozenka){
        if(ozenka>=2 && ozenka<=5){
            ozenki.add(ozenka);
            GET_STUDENT_SUM+=ozenka;
            this.i++;
            GET_STUDENT_COUNT++;
        }
    }

    public double getAvgMark(){
        int sum=0;
        for(int x:this.ozenki){
            sum+=x;
        }
        return 1.0*sum/this.i;
    }

    public void soutOzeni(){
        for(Integer x: ozenki){
            System.out.print(x+" ");
        }
    }



    public void removeOzenki(){
        for(int i=0;i<ozenki.size();){
            ozenki.remove(i);
        }
    }

    public String toString(){
        String itog="";
        itog+=this.id+"_"+this.name+"_"+this.getGroup();
        if(ozenki==null || ozenki.size()==0) {
            return itog;
        }else{
            for (int i = 0; i < ozenki.size(); i++) {
                itog += "_" + ozenki.get(i);
            }
        }
        return itog;
    }

    public static Student fromString(String inp){
        Student s = new Student();
        String[] arr = inp.split("_");
        s.setId(Integer.parseInt(arr[0]));
        s.setName(arr[1]);
        s.setGroup(arr[2]);
        s.spawnOzenki();
        for (int i = 3; i < arr.length; i++) {
            s.ozenki.add(Integer.parseInt(arr[i]));
        }
        return s;
    }
}

