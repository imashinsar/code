create or replace PROCEDURE add_category(title varchar2)
IS 
sq int;
TITLE_IS_NULL EXCEPTION;
BEGIN 
    IF title IS NULL THEN 
        RAISE TITLE_IS_NULL;
    END IF;
    sq := category_sq.NEXTVAL;
    INSERT INTO category(ID, name) VALUES(sq, title);
    EXCEPTION
        WHEN TITLE_IS_NULL THEN
            RAISE_APPLICATION_ERROR(-20001, 'Имя null');
END;







create or replace PROCEDURE add_customer(name_val VARCHAR2, email_val VARCHAR2, password_var VARCHAR2, money_val int)
IS 
sq_value        int;
INVALID_VALUES  EXCEPTION; 
NEGATIVE_MONEY  EXCEPTION;
BEGIN 
    IF name_val IS NULL OR email_val IS NULL OR password_var IS NUll
        THEN RAISE INVALID_VALUES;
    END IF;
        IF  money_val<0
            THEN RAISE NEGATIVE_MONEY;
        END IF;
            sq_value  := CUSTOMER_SQ.NEXTVAL;
    INSERT INTO customer(ID, name, email, password, money) VALUES(sq_value, name_val, email_val, password_var, money_val);
    EXCEPTION
        WHEN INVALID_VALUES THEN
            RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
            RETURN;
        WHEN DUP_VAL_ON_INDEX THEN
            RAISE_APPLICATION_ERROR(-20001, 'Такой email уже есть');
            RETURN;
        WHEN NEGATIVE_MONEY THEN
            RAISE_APPLICATION_ERROR(-20001, 'Отрицательный баланс');
        WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20001, 'ХЗ');
            RETURN;
END;








create or replace PROCEDURE add_ordering(customer_id_val int, status_id_val int, amount_val int, product_id_val int)
IS    sq          int;
total_price_ins   int;
INVALID_VALUES    EXCEPTION;
INVALID_CUSTOMER  EXCEPTION;
INVALID_STATUS    EXCEPTION;
NEGATIVE_AMOUNT   EXCEPTION;
INVALID_PRODUCT   EXCEPTION;
product_price product.price%TYPE;
CURSOR product_availability_curs IS (SELECT COUNT(*)
                    FROM product
                    WHERE ID = product_id_val);
    product_counter  int;
CURSOR customer_curs  IS (SELECT COUNT(*)
                    FROM customer
                    WHERE ID = customer_id_val);
    customer_counter int;
CURSOR status_curs  IS (SELECT COUNT(*)
                    FROM ordering_status
                    WHERE ID = status_id_val);
    status_counter   int;
BEGIN
    IF customer_id_val IS NULL OR
       status_id_val   IS NULL OR
       amount_val      IS NULL
        THEN RAISE INVALID_VALUES;
    END IF;

        OPEN customer_curs;
            FETCH customer_curs INTO customer_counter;
                IF customer_counter = 0 THEN
                    RAISE INVALID_CUSTOMER;
                END IF;
        CLOSE customer_curs;

        OPEN status_curs;
            FETCH status_curs INTO status_counter;
                IF status_counter = 0 THEN
                    RAISE INVALID_STATUS;
                END IF;
        CLOSE status_curs;

        OPEN product_availability_curs;
            FETCH product_availability_curs INTO product_counter;
                IF product_counter = 0 THEN
                    RAISE INVALID_PRODUCT;
                END IF;
        CLOSE product_availability_curs;

        sq := ordering_sq.NEXTVAL;

        SELECT price INTO product_price FROM product WHERE ID =product_id_val;

        total_price_ins := amount_val*product_price;


        INSERT INTO ordering VALUES(sq, total_price_ins, customer_id_val, status_id_val, amount_val);

        add_ordering_item(sq, product_id_val);

EXCEPTION 
    WHEN INVALID_VALUES THEN
            RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
                RETURN;
    WHEN INVALID_CUSTOMER THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неверный customer');
                RETURN;
    WHEN INVALID_STATUS THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неверный status');
                RETURN;
    WHEN NEGATIVE_AMOUNT THEN
            RAISE_APPLICATION_ERROR(-20000, 'Отрицательный amount');
                RETURN;
    WHEN INVALID_PRODUCT THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неверный product');
                RETURN;


END;











create or replace PROCEDURE add_ordering_item(ordering_id_val int, product_id_val int)
IS sq               int;
INVALID_VALUES      EXCEPTION;
INVALID_ORDERING_ID EXCEPTION;
INVALID_PRODUCT_ID  EXCEPTION;
CURSOR odering_curs  IS (SELECT COUNT(*)
                    FROM ordering
                    WHERE ID = ordering_id_val);
            ordering_counter  int;
CURSOR product_curs  IS (SELECT COUNT(*)
                    FROM product
                    WHERE ID = product_id_val);
            product_counter   int;
BEGIN
    IF ordering_id_val IS NULL OR
       product_id_val  IS NULL
            THEN RAISE INVALID_VALUES;
    END IF;

        OPEN odering_curs;
            FETCH odering_curs INTO ordering_counter;
                IF ordering_counter = 0 THEN
                    RAISE INVALID_ORDERING_ID;
                END IF;
        CLOSE odering_curs;

        OPEN product_curs;
            FETCH product_curs INTO product_counter;
                IF product_counter = 0 THEN
                    RAISE INVALID_PRODUCT_ID;
                END IF;
        CLOSE product_curs;

    sq := ordering_item_sq.NEXTVAL;
    INSERT INTO ordering_item VALUES(sq, product_id_val, ordering_id_val);
    EXCEPTION
        WHEN INVALID_VALUES THEN
            RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
                RETURN;
        WHEN INVALID_ORDERING_ID THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неправильный ordering_id');
                RETURN;
        WHEN INVALID_PRODUCT_ID THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неправильныц product_id');
                RETURN;
END; 










create or replace PROCEDURE add_ordering_status(title varchar2)
IS 
sq int;
TITLE_IS_NULL EXCEPTION;
BEGIN 
    IF title IS NULL THEN 
        RAISE TITLE_IS_NULL;
    END IF;
    sq := ORDERING_STATUS_SQ.NEXTVAL;
    INSERT INTO ordering_status(ID, name) VALUES(sq, title);
    EXCEPTION
        WHEN TITLE_IS_NULL THEN
            RAISE_APPLICATION_ERROR(-20001, 'Имя null');
END;










create or replace PROCEDURE add_product(name_val VARCHAR2, 
                             price_val int, 
                             amount_val int, 
                             supply_id_val int, 
                             status_id_val int, 
                             category_id_val int)
IS      sq                int;
        INVALID_VALUES    EXCEPTION;
        NEGATIVE_PRICE    EXCEPTION;
        NEGATIVE_AMOUNT   EXCEPTION;
        INVALID_SUPPLY    EXCEPTION;
        INVALID_STATUS    EXCEPTION;
        INVALID_CATEGORY  EXCEPTION;
        CURSOR supply_curs IS
            (SELECT COUNT(*)
                    FROM supply
                    WHERE ID = supply_id_val);
        CURSOR status_curs IS
            (SELECT COUNT(*)
                    FROM product_status
                    WHERE ID = status_id_val);
        CURSOR category_curs IS
            (SELECT COUNT(*)
                    FROM category
                    WHERE ID = category_id_val);

        supply_counter   int;
        status_counter   int;
        category_counter int;
BEGIN
    IF name_val IS NULL OR
       price_val IS NULL OR
       supply_id_val IS NULL OR
       status_id_val IS NULL OR
       category_id_val IS NULL   
            THEN  RAISE INVALID_VALUES;
    END IF;

        IF price_val<0 
            THEN  RAISE NEGATIVE_PRICE;
        END IF;
        IF amount_val<0
            THEN RAISE NEGATIVE_AMOUNT;
        END IF;


        OPEN supply_curs;
            FETCH supply_curs INTO supply_counter;
                IF supply_counter = 0 THEN
                    RAISE INVALID_SUPPLY;
                END IF;
        CLOSE supply_curs;

        OPEN status_curs;
            FETCH status_curs INTO status_counter;
                IF status_counter = 0 THEN
                    RAISE INVALID_STATUS;
                END IF;
        CLOSE status_curs;

        OPEN category_curs;
            FETCH category_curs INTO category_counter;
                IF category_counter = 0 THEN
                    RAISE INVALID_CATEGORY;
                END IF;
        CLOSE category_curs;

        sq := product_sq.NEXTVAL;
    INSERT INTO product VALUES(sq, name_val, price_val, amount_val, supply_id_val, status_id_val, category_id_val);

    EXCEPTION 
        WHEN INVALID_VALUES THEN
            RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
                RETURN;
        WHEN NEGATIVE_PRICE THEN
            RAISE_APPLICATION_ERROR(-20000, 'Отрицательный price');
                RETURN;
        WHEN NEGATIVE_AMOUNT THEN
            RAISE_APPLICATION_ERROR(-20000, 'Отрицательный amount');
                RETURN;
        WHEN INVALID_SUPPLY THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неправильный supply');
                RETURN;
        WHEN INVALID_STATUS THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неправильный status');
                RETURN;
        WHEN INVALID_CATEGORY THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неправильный category');
                RETURN;
END;






create or replace PROCEDURE add_product_status(title varchar2)
IS 
sq int;
TITLE_IS_NULL EXCEPTION;
BEGIN 
    IF title IS NULL THEN 
        RAISE TITLE_IS_NULL;
    END IF;
    sq := product_status_SQ.NEXTVAL;
    INSERT INTO product_status(ID, name) VALUES(sq, title);
    EXCEPTION
        WHEN TITLE_IS_NULL THEN
            RAISE_APPLICATION_ERROR(-20001, 'Имя null');
END;














create or replace PROCEDURE add_supplier(title varchar2)
IS 
sq int;
TITLE_IS_NULL EXCEPTION;
BEGIN 
    IF title IS NULL THEN 
        RAISE TITLE_IS_NULL;
    END IF;
    sq := supplier_sq.NEXTVAL;
    INSERT INTO supplier(ID, name) VALUES(sq, title);
    EXCEPTION
        WHEN TITLE_IS_NULL THEN
            RAISE_APPLICATION_ERROR(-20001, 'Имя null');
END;








create or replace PROCEDURE add_supply(amount_val int, price_val int, supplier_id_val int)
IS    sq          int;
INVALID_VALUES    EXCEPTION;
NEGATIVE_AMOUNT   EXCEPTION;
NEGATIVE_PRICE    EXCEPTION;
INVALID_SUPPLIER  EXCEPTION;
supplier_counter  int;
CURSOR supplier_counter_curs  IS (SELECT COUNT(*)
                    FROM supplier
                    WHERE ID = supplier_id_val);
BEGIN 
    IF amount_val IS NULL OR price_val IS NULL OR supplier_id_val IS NULL
        THEN RAISE INVALID_VALUES;
    END IF;

        IF amount_val <0 THEN RAISE NEGATIVE_AMOUNT; END IF;
        IF price_val  <0 THEN RAISE NEGATIVE_PRICE;  END IF;

        OPEN supplier_counter_curs;
        FETCH supplier_counter_curs INTO supplier_counter;

            IF supplier_counter = 0 THEN
                RAISE INVALID_SUPPLIER;
            END IF;
        CLOSE supplier_counter_curs;
    sq := supply_sq.NEXTVAL;
    INSERT INTO supply VALUES(sq, amount_val, price_val, supplier_id_val);

EXCEPTION
    WHEN INVALID_VALUES THEN
        RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
            RETURN;
    WHEN NEGATIVE_AMOUNT THEN
        RAISE_APPLICATION_ERROR(-20000, 'Количество отрицательно');
            RETURN;
    WHEN NEGATIVE_PRICE THEN
        RAISE_APPLICATION_ERROR(-20000, 'Цена отрицательна');
            RETURN;
    WHEN INVALID_SUPPLIER THEN
        RAISE_APPLICATION_ERROR(-20000, 'Нет такого поставщика');
            RETURN;
END;






create or replace PROCEDURE add_transaction(money_val int, customer_id_val int, ordering_id_val int)
IS   sq           int;
INVALID_VALUES    EXCEPTION;
NEGATIVE_MONEY    EXCEPTION;
INVALID_CUSTOMER  EXCEPTION;
INVALID_ORDERING  EXCEPTION;
CURSOR customer_curs  IS (SELECT COUNT(*)
                    FROM customer
                    WHERE ID = customer_id_val);
                customer_counter int;
CURSOR ordering_curs  IS (SELECT COUNT(*)
                    FROM ordering
                    WHERE ID = ordering_id_val);
                ordering_counter int;
BEGIN
    IF money_val    IS NULL OR
       customer_id_val IS NULL OR
       ordering_id_val IS NULL 
        THEN RAISE INVALID_VALUES;
    END IF;

    IF money_val<0 
        THEN RAISE NEGATIVE_MONEY;
    END IF;

    OPEN customer_curs;
        FETCH customer_curs INTO customer_counter;

            IF customer_counter = 0 THEN
                RAISE INVALID_CUSTOMER;
            END IF;
    CLOSE customer_curs;

    OPEN ordering_curs;
        FETCH ordering_curs INTO ordering_counter;

            IF ordering_counter = 0 THEN
                RAISE INVALID_ORDERING;
            END IF;
    CLOSE ordering_curs;

    sq:= transaction_sq.NEXTVAL;
    INSERT INTO transaction VALUES(sq, money_val, customer_id_val, ordering_id_val);


    EXCEPTION 
    WHEN INVALID_VALUES THEN
            RAISE_APPLICATION_ERROR(-20000, 'Что-то нулл');
                RETURN;
    WHEN NEGATIVE_MONEY THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неверный money');
                RETURN;
    WHEN INVALID_CUSTOMER THEN
            RAISE_APPLICATION_ERROR(-20000, 'Неверный customer');
                RETURN;
    WHEN INVALID_ORDERING THEN
            RAISE_APPLICATION_ERROR(-20000, 'Отрицательный ordering');
                RETURN;
END;
