import java.util.Scanner;
import java.util.Random;
public class Main {
    public static Scanner scan = new Scanner(System.in);
    public static Random rand = new Random();

    public static void main(String[] args) {
        double rez=0.0;
        while(true){
            System.out.println("Результат: "+ rez);
            System.out.println("Выберите операцию: ");
            System.out.println(" + - * / C");
            String inp=scan.nextLine();
            if(!(inp.charAt(0)=='+'||inp.charAt(0)=='/'||inp.charAt(0)=='*'||inp.charAt(0)=='-'||inp.charAt(0)=='c'||inp.charAt(0)=='C')){
                System.out.println("Вы не сделали действие");
                continue;
            }
            if(inp.charAt(0)=='+'){
                rez+=peredelat(inp);
                rez=(int)(rez*100000)/100000.0;

            }
            if(inp.charAt(0)=='-'){
                rez-=peredelat(inp);
                rez=(int)(rez*100000)/100000.0;
            }
            if(inp.charAt(0)=='*'){
                rez*=peredelat(inp);
            }
            if(inp.charAt(0)=='/' && peredelat(inp)==0){
                System.out.println("На ноль делить нельзя");
                rez+=0.0;
                continue;
            }
            if(inp.charAt(0)=='/'){
                rez/=peredelat(inp);
            }
            if(inp.charAt(0)=='C' || inp.charAt(0)=='c'){
                System.out.println("Обнуление");
                rez=0.0;
            }
        }
    }


    public static double peredelat(String inp){
        double a=0.0;
        String chis="";
        int indZnakov=0, indTochek=0;
        for(int i=0;i<inp.length();i++){
            char sym=inp.charAt(i);
            if(sym=='.'){
                indTochek++;
            }
            if(!(sym=='C'|| sym=='c'|| sym=='-' || sym=='+' || sym=='*' || sym=='/' || (sym>='0' && sym<='9')|| sym=='.') ){
                System.out.println("Только знаки выражений и числа");
                return 0.0;
            }
            chis+=sym;
            if(sym=='-' || sym=='+' || sym=='*' || sym=='/' ){
                indZnakov++;
                chis="";
            }

            if(indZnakov==2){
                System.out.println("Слишком много действий");
                return 0.0;
            }
            if(indTochek==2){
                System.out.println("Слишком много точек");
                return 0.0;
            }
            if(chis.length()>=1){
                a = Double.parseDouble(chis);
            }
        }
        return a;
    }
}
